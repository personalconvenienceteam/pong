#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <cab202_graphics.h>
#include <cab202_sprites.h>
#include <cab202_timers.h>

#define DELAY (10) /* Milliseconds between game updates */

sprite_id ball, singularity; sprite_id *rails;
int rail_length, top_rail_y, bottom_rail_y;
int score, lives, level, game_loop, current_minutes;
int paddle_length, player_paddle_position, computer_paddle_position;
int current_seconds, levelThreeTime; 
bool game_over = false, update_screen = true;
 
char * help_box_img = 
/**/ " ---------------------- "
/**/ "| CAB-PONG ULTRA 2016  |"
/**/ "|Roderick Lenz N9428157|"
/**/ " ---------------------- "
/**/ "|Keyboard Commands:    |"
/**/ "|'a' key: Paddle up    |"
/**/ "|'s' key: Paddle down  |"
/**/ "|'l' key: Advance Level|"
/**/ "|'h' key: Help Screen  |"
/**/ "|'q' key: Quit game    |"
/**/ " ---------------------- " 
/**/ " Press Any Key to Begin ";

char * count_down_box_img =
/**/ "x-----------------------x"
/**/ "|Get ready! Starting in:|"
/**/ "|                       |"
/**/ "|                       |"
/**/ "x-----------------------x";

char * game_over_box_img =
/**/ "x-----------------------x"
/**/ "|       GAME OVER!      |"
/**/ "|     SCORE:            |"
/**/ "|      Play again?      |"
/**/ "|         Y/N           |"
/**/ "x-----------------------x";

char * singularity_img =
/**/ "S  S  S"
/**/ " s s s "
/**/ "Ss @ sS"
/**/ " s s s "
/**/ "S  S  S";

void initialize_variables(){
	score = 0;
	lives = 1;
	level = 1;
	game_loop = 0;
	current_minutes=0;
	player_paddle_position = 0;
	computer_paddle_position = 0;
	current_seconds = 0;
	levelThreeTime = 500;
}

void display_help_screen(void){
	clear_screen();
	sprite_id help_box;
	int help_box_height = 12;
	int help_box_width = strlen(help_box_img) / help_box_height;
	int help_box_y_location = (screen_height() - help_box_height) / 2;
	int help_box_x_location = (screen_width() - help_box_width) / 2;	
	help_box = sprite_create(help_box_x_location, help_box_y_location, help_box_width, help_box_height, help_box_img);
	sprite_draw(help_box);
	show_screen();
	wait_char();
	}

void update_time(){
	game_loop++;
	if(game_loop==100){
		current_seconds++;
		game_loop = 0;
	}
	if(current_seconds==60){
		current_minutes++;
		current_seconds = 0;
		game_loop = 0;
	}
}
 
void draw_game_screen(){
	clear_screen();
	int info_box_height = 2;
	// Set coordinates for lives, score, level and game time
	int info_box_text_y = 1, lives_text_x_position = 2, score_text_x_position = round(screen_width() / 4);
	int level_text_x_position = round(screen_width() / 2), game_time_text_x_position = 3 * score_text_x_position;
	int screen_left = 0, screen_right = screen_width() - 1;
	int screen_top = 0, screen_bottom = screen_height() - 1;	
	char border_char = '*';
	
	// Draw borders
	draw_line(screen_left, screen_top, screen_right, screen_top, border_char);
	draw_line(screen_right, screen_top, screen_right, screen_bottom, border_char);
	draw_line(screen_right, screen_bottom, screen_left, screen_bottom, border_char);
	draw_line(screen_left, screen_bottom, screen_left, screen_top, border_char);
	draw_line(screen_left, info_box_height, screen_right, info_box_height, border_char);
	
	// Draw info in box
	// Current format best for screen width greater than 46
	draw_formatted(lives_text_x_position, info_box_text_y, "Lives: %d", lives);
	draw_formatted(score_text_x_position, info_box_text_y, "Score: %d", score);
	draw_formatted(level_text_x_position, info_box_text_y, "Level: %d", level);
	draw_formatted(game_time_text_x_position, info_box_text_y, "Time: %d:%02d", current_minutes, current_seconds);
	}
	
	void display_count_down(){
		sprite_id count_down_box;
		int box_height = 5;
		int box_width = strlen(count_down_box_img)/box_height;
		int box_x_position = (screen_width()-box_width)/2;
		int box_y_position = (screen_height() - box_height )/2 + 2;
		int timer_value = 3;
		count_down_box = sprite_create(box_x_position, box_y_position, box_width, box_height, count_down_box_img);
		sprite_draw(count_down_box);
		show_screen();
		while(timer_value>0){
			draw_formatted(screen_width()/2, (screen_height()/2) + 2, "%d", timer_value);
			timer_pause (300);
			timer_value -= 1;
			show_screen();
		}
	}
	
	void setup_paddles (void){					
		if (screen_height()>=21){
			paddle_length = 7;
		}else{
			paddle_length = (screen_height() - 3)/2;
		}
	}

void draw_paddles(bool player_paddle){
	int paddle_x, paddle_y_start, paddle_y_end, paddle_position;
	
	if(player_paddle){
		paddle_x = screen_width() - 4;
		paddle_position = player_paddle_position;
	}else{
		paddle_x = 3;
		paddle_position = sprite_y(ball)-((screen_height() + 4 - paddle_length)/2);
	}
	
	paddle_y_start = ((screen_height() + 3 - paddle_length)/2) + paddle_position;
	
	if (paddle_y_start < 3){
		paddle_y_start = 3;
	}else if (paddle_y_start > (screen_height() - paddle_length - 1)){
		paddle_y_start = screen_height() - paddle_length - 2;
	}
	
	paddle_y_end = paddle_y_start + paddle_length - 1;

	draw_line(paddle_x, paddle_y_start, paddle_x, paddle_y_end, '|');		
}
	
void setup_ball(){
	int now = get_current_time();
	srand(now);

	int screen_center_x = screen_width() / 2;
	int screen_center_y = (screen_height() + 2) /2;	
	ball = sprite_create(screen_center_x, screen_center_y, 1, 1, "O");
	sprite_turn_to(ball, 0.2, 0);
	int angle = rand() %90 - 45;
	sprite_turn(ball, angle);
}

void setup_singularity(){
	levelThreeTime = 500;
	int singularity_height = 5;
	int singularity_width = strlen(singularity_img)/singularity_height;
	int singularity_x_position = (screen_width()-singularity_width)/2;
	int singularity_y_position = (screen_height() - singularity_height )/2 + 2;
	
	singularity = sprite_create(singularity_x_position, singularity_y_position, singularity_width,singularity_height, singularity_img);
	}

bool check_paddle_collision(int x_or_y){
	int paddle_y_start = ((screen_height() + 3 - paddle_length)/2) + player_paddle_position;
	int paddle_y_end = paddle_y_start + paddle_length - 1;
	int ball_x = round(sprite_x(ball));
	int ball_y = round(sprite_y(ball));
	double ball_dy=sprite_dy(ball);
	
	if (x_or_y == 'x' && ball_y <= paddle_y_end && ball_y >= paddle_y_start && ball_x == screen_width() - 5){
			return true;
		}
	if (x_or_y == 'y' && ball_y == paddle_y_end+1 && ball_dy < 0 && (ball_x==screen_width()-4||ball_x==screen_width()-5)){
		if (ball_y + 1 < screen_height()-1){
		sprite_move_to(ball, ball_x, ball_y + 1);
		}
		return true;
	}else if (x_or_y == 'y' && ball_y == paddle_y_start-1 && ball_dy > 0 && (ball_x==screen_width()-4||ball_x==screen_width()-5)){
		if (ball_y - 1 > 2){
		sprite_move_to(ball, ball_x, ball_y - 1);
		}
		return true;
	}
	
	return false;
}

void setup_rails(){
	rail_length = screen_width();
	top_rail_y = (screen_height()-3)/3;
	bottom_rail_y = top_rail_y*2;
	int rail_x_start = (screen_width()-(rail_length/2))/2;
	
	rails=calloc(rail_length, sizeof(sprite_id));
	
	for (int i = 0; i<rail_length/2; i++){
			rails[i]=sprite_create(rail_x_start+i, top_rail_y, 1, 1, "=");
	}
	for (int i=rail_length/2; i<rail_length; i++){
	rails[i]=sprite_create(rail_x_start+(i-rail_length/2), bottom_rail_y, 1, 1, "=");	
	}
	
}

void setup(){
		clear_screen();
		draw_game_screen();
		setup_paddles();
		draw_paddles(true);
		setup_ball();
		setup_singularity();
		setup_rails();
		show_screen();
		display_count_down();
}

void exit_game(void){
	sprite_id game_over_box;
	int box_height = 6;
	int box_width = strlen(game_over_box_img)/box_height;
	int box_x_position = (screen_width()-box_width)/2;
	int box_y_position = (screen_height() - box_height )/2 + 2;
	game_over_box = sprite_create(box_x_position, box_y_position, box_width, box_height, game_over_box_img);
	sprite_draw(game_over_box);
	draw_formatted(screen_width()/2 + 1, (screen_height()/2) + 1, "%d", score);
	show_screen();
	int key;
	do{
		key=get_char();
	}while(key!='y' && key!='n');
	if (key=='y'){
		initialize_variables();
		for (int i=0; i<rail_length; i++){
			sprite_show(rails[i]);
		}
		display_help_screen();
		setup();
	}else if(key=='n'){
	game_over = true;
	clear_screen();
	draw_formatted(0,0,"Thanks for playing");
	show_screen();
	wait_char();
	}
}

void ball_out(){
	if (lives>0){
	lives--;
	setup();
	} else {
		exit_game();
	}
}

void draw_singularity(){
		if (levelThreeTime>0){
			levelThreeTime--;
			return;
		}else{
			sprite_draw(singularity);
		}
}

void accelerate_towards_singularity(){
	double GM;
	if (hypot(screen_width(), screen_height())<100){
		GM=0.25;
	}else{
		GM=0.5;
	}
	if (levelThreeTime == 0){
	double x_diff = (sprite_x(singularity)+3) - sprite_x(ball);
	double y_diff = (sprite_y(singularity)+2) - sprite_y(ball);
	double dist_squared = (x_diff*x_diff) + (y_diff*y_diff);
	
	if (dist_squared<5){
		dist_squared = 5;
	}

	double dist=sqrt(dist_squared);
	double dx = sprite_dx(ball);
	double dy = sprite_dy(ball);
	double a = GM/dist_squared;

	dx = dx + (a*x_diff/dist);
	dy = dy + (a*y_diff/dist);
	
	double v = hypot(dx,dy);

	if (v>1){
		dx = dx/v;
		dy = dy/v;
		}
	
	sprite_turn_to(ball, dx, dy);
	}
}

void check_rail_collision(){
	int ball_x=round(sprite_x(ball));
	int ball_y=round(sprite_y(ball));
	double ball_dy = sprite_dy(ball);
	double ball_dx = sprite_dx(ball);
	int rail_x;
	if (ball_y==top_rail_y){
	for (int i=0; i<rail_length/2; i++){
		rail_x=sprite_x(rails[i]);
		if (ball_x==rail_x&&sprite_visible(rails[i])){
			ball_dy=-ball_dy;
			sprite_hide(rails[i]);
			if (i-1>=0){
			sprite_hide(rails[i-1]);
			}			
			if (i+1<rail_length/2){
				sprite_hide(rails[i+1]);
			}
		}
	}
	}else if (ball_y==bottom_rail_y){
	for (int i=rail_length/2; i<rail_length; i++){
		rail_x=sprite_x(rails[i]);
		if (ball_x==rail_x&&sprite_visible(rails[i])){
			ball_dy=-ball_dy;
			sprite_hide(rails[i]);
			if (i-1>=rail_length/2){
			sprite_hide(rails[i-1]);
			}			
			if (i+1<rail_length){
				sprite_hide(rails[i+1]);
			}
		}
	}
	}
	sprite_turn_to(ball, ball_dx, ball_dy);
}
void update_ball(){
	sprite_step(ball);
	
	int ball_x = round( sprite_x( ball ) );
	int ball_y = round( sprite_y( ball ) );
	
	double ball_dx = sprite_dx(ball);
	double ball_dy = sprite_dy(ball);

	if (ball_x<=0 || (level != 1 && ball_x <=3)){
		ball_dx = -ball_dx;
	}
	
	if (ball_y<=2||ball_y>=screen_height() - 1){
		ball_dy = -ball_dy;
	}
	
	bool paddle_collision = check_paddle_collision('x');
	if (paddle_collision){
		ball_dx = -ball_dx;
		score++;
	}
	
	bool end_collision = check_paddle_collision('y');
	if (end_collision){
		ball_dy = -ball_dy;
	}
	
	if (ball_dx != sprite_dx(ball) || ball_dy != sprite_dy(ball)){
		sprite_back(ball);
		sprite_turn_to(ball, ball_dx, ball_dy);
	}	

	if (level == 3){
		accelerate_towards_singularity();
		}
	if (level == 4){
		check_rail_collision();
	}
}

void update_paddles (int key){
	int paddle_y_start = (screen_height() + 3 - paddle_length)/2;
	int paddle_y_end = paddle_y_start + paddle_length - 1;
	if (key == 'w' && (paddle_y_start + player_paddle_position) > 3){
		player_paddle_position--;
	} else if (key == 's' && paddle_y_end + player_paddle_position < screen_height()- 2){
		player_paddle_position++;
	}
}

void draw_rails(){
	for (int i=0; i<rail_length; i++){
		sprite_draw(rails[i]);
	}
}

void process(void){
	int key = get_char();

	if (key == 'h'){
		display_help_screen();
	} else if (key == 'q') {
		exit_game();
	} else if (key == 'l'){
		level = level%4;
		level++;
		levelThreeTime=500;
		for (int i=0; i<rail_length; i++){
			sprite_show(rails[i]);
		}
	}
	
	update_paddles(key);
	update_ball();
	update_time();
	draw_game_screen();
	draw_paddles(true);

	if (level > 1){
		draw_paddles(false);
	}
	if (level==3){
		draw_singularity();
	}
	if (level==4){
		draw_rails();
	}

	sprite_draw(ball);
    if (sprite_x(ball)>=screen_width()-1){
		ball_out();
	}
}


int main(void){
	initialize_variables();
	setup_screen();
	display_help_screen();
	setup();
	
	while (!game_over){
		process();
		show_screen();
		timer_pause(DELAY);
	}

	}